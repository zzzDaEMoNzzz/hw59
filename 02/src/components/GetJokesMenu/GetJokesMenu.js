import React, { PureComponent } from 'react';
import './GetJokesMenu.css';

class GetJokesMenu extends PureComponent {
  render() {
    return (
      <div className="GetJokesMenu">
        <button onClick={() => this.props.getJokes(this.props.jokesCount)}>Get jokes</button>
      </div>
    );
  }
}

export default GetJokesMenu;
