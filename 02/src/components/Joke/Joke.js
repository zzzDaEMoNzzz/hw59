import React from 'react';
import './Joke.css';

const Joke = props => {
  return (
    <div className="Joke">
      {props.text}
    </div>
  );
};

export default Joke;
