import React, {Component} from 'react';
import './Jokes.css';
import GetJokesMenu from "../../components/GetJokesMenu/GetJokesMenu";
import Joke from "../../components/Joke/Joke";

const JOKES_COUNT = 5;

class Jokes extends Component {
  state = {
    jokes: []
  };

  getJoke = () => {
    return fetch('https://api.chucknorris.io/jokes/random').then(response => {
      if (response.ok) return response.json();

      throw new Error('[getJoke] Something wrong with request');
    }).catch(error => console.log(error));
  };

  getJokes = count => {
    const promises = [];
    for (let i = 0; i < count; i++) {
      const promise = this.getJoke();
      if (promise) {
        promises.push(promise);
      }

    }

    Promise.all(promises).then(jokes => {
      const jokesArray = jokes.map(joke => {
        return {text: joke.value}
      });

      this.setState({jokes: jokesArray});
    });
  };

  componentDidMount() {
    this.getJokes(JOKES_COUNT);
  }

  render() {
    const jokesArray = this.state.jokes.reduce((jokes, joke) => {
      jokes.push(
        <li key={jokes.length}><Joke text={joke.text}/></li>
      );

      return jokes;
    }, []);

    return (
      <div className="Jokes">
        <GetJokesMenu getJokes={this.getJokes} jokesCount={JOKES_COUNT}/>
        <ol className="Jokes-list">
          {jokesArray}
        </ol>
      </div>
    );
  }
}

export default Jokes;
