import React, { Component } from 'react';
import './FilmItem.css';

class FilmItem extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.name !== this.props.name;
  };

  render() {
    return (
      <div className="FilmItem">
        <input
          type="text"
          value={this.props.name}
          className="FilmItem-input"
          onChange={event => this.props.changeFilmName(event.target.value, this.props.id)}
        />
        <button
          className='FilmItem-removeBtn'
          onClick={() => this.props.deleteFilm(this.props.id)}
        >
          X
        </button>
      </div>
    );
  }
}

export default FilmItem;
