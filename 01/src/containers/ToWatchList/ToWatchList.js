import React, {Component} from 'react';
import './ToWatchList.css';
import Button from "../../components/UI/Button/Button";
import FilmItem from "../../components/FilmItem/FilmItem";

class ToWatchList extends Component {
  state = {
    films: [],
    currentFilmName: ''
  };

  constructor(props) {
    super(props);

    const filmsJSON = localStorage.getItem('ToWatchList');
    if (filmsJSON) this.state.films = JSON.parse(filmsJSON);
  }

  componentDidUpdate() {
    localStorage.setItem('ToWatchList', JSON.stringify(this.state.films));
  }

  addItem = (event, filmName) => {
    event.preventDefault();

    let films = [...this.state.films];

    films.push({
      id: new Date().getTime(),
      name: filmName
    });

    this.setState({
      films,
      currentFilmName: ''
    });
  };

  deleteItem = filmID => {
    const filmIndex = this.state.films.findIndex(film => film.id === filmID);

    if (filmIndex !== -1) {
      const films = [...this.state.films];
      films.splice(filmIndex, 1);

      this.setState({films});
    }
  };

  currentFilmNameChange = currentValue => {
    this.setState({
      currentFilmName: currentValue
    });
  };

  toWatchListFilmNameChange = (currentValue, filmID) => {
    const filmIndex = this.state.films.findIndex(film => film.id === filmID);

    if (filmIndex !== -1) {
      const films = [...this.state.films];
      const currentFilm = {...films[filmIndex]};
      currentFilm.name = currentValue;
      films[filmIndex] = currentFilm;

      this.setState({films});
    }
  };

  render() {
    const toWatchList = this.state.films.reduce((filmsList, film) => {
      filmsList.push(
        <FilmItem
          key={film.id}
          id={film.id}
          name={film.name}
          changeFilmName={this.toWatchListFilmNameChange}
          deleteFilm={this.deleteItem}
        />
      );

      return filmsList;
    }, []);

    return (
      <div className="ToWatchList">
        <form className="ToWatchList-addMenu" onSubmit={(event) => this.addItem(event, this.state.currentFilmName)}>
          <input
            type="text"
            onChange={event => this.currentFilmNameChange(event.target.value)}
            value={this.state.currentFilmName}
            required={true}
          />
          <Button>Add</Button>
        </form>
        <p>To watch list:</p>
        {toWatchList}
      </div>
    );
  }
}

export default ToWatchList;
